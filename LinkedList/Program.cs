﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
namespace LinkedList
{
    /// <summary>
    /// This console application creates and inserts values in linked list
    /// It fetches the number on the 5th element from the end 
    /// </summary>
    class SingleLinkedlist
    {
        private int data;
        private SingleLinkedlist _next;
        public SingleLinkedlist()
        {
            data = 0;
            _next = null;
        }
        public SingleLinkedlist(int value)
        {
            data = value;
            System.Console.WriteLine("Inserting " + value);
            _next = null;
        }
        public SingleLinkedlist InsertNext(int value)
        {

            SingleLinkedlist node = new SingleLinkedlist(value);
            if (this._next == null)
            {
                node._next = null;
                this._next = node;
            }
            else
            {
                SingleLinkedlist temp = this._next;
                node._next = temp;
                this._next = node;
            }
            return node;
        }
     public void Traverse(SingleLinkedlist node)
        {
            if (node == null)
                node = this;
            System.Console.WriteLine("Traversing Singly Linked List :");
            while (node != null)
            {
                System.Console.WriteLine("This is the 5th element of linked list");
                System.Console.WriteLine(node.data);
                node = node._next;
            }
        }
    }
    class Program
    {
        static void Main(string[] args)
        {
            SingleLinkedlist node1 = new SingleLinkedlist(1);
            SingleLinkedlist node2 = node1.InsertNext(2);
            SingleLinkedlist node3 = node2.InsertNext(3);
            SingleLinkedlist node4 = node3.InsertNext(4);
            SingleLinkedlist node5 = node4.InsertNext(5);
            SingleLinkedlist node6 = node5.InsertNext(6);
            SingleLinkedlist node7 = node6.InsertNext(7);
            SingleLinkedlist node8 = node7.InsertNext(8);
           
            node2.Traverse(node5);
            System.Console.ReadLine();
        }
    }
}